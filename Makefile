SHELL = /bin/bash

build_tag ?= docker-bundle

ruby_version ?= 2.6

.PHONY: build
build:
	docker build -t $(build_tag) --build-arg ruby_version=$(ruby_version) .

.PHONY: clean
clean:
	rm -f test/Gemfile.lock

.PHONY: test
test: clean test/Gemfile.lock

test/Gemfile.lock:
	docker run --rm \
		-v "$(shell pwd)/test:/usr/src/app" \
		-u "$(shell id -u):0" \
		$(build_tag) lock
