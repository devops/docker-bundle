# docker-bundle

    $ docker run --rm -u "$(id -u):0" \
		-v "$(pwd)/Gemfile:/usr/src/app/Gemfile" \
		-v "$(pwd)/Gemfile.lock:/usr/src/app/Gemfile.lock" \
		gitlab-registry.oit.duke.edu/devops/docker-bundle:ruby-2.7 \
		[BUNDLE COMMAND: lock, update, etc.]

To cache gems, mount a volume at `/usr/local/bundle` (`$GEM_HOME`):

	$ docker volume create bundle_cache

    $ docker run --rm -u "$(id -u):0" \
		-v "$(pwd)/Gemfile:/usr/src/app/Gemfile" \
		-v "$(pwd)/Gemfile.lock:/usr/src/app/Gemfile.lock" \
		-v bundle_cache:/usr/local/bundle \
		gitlab-registry.oit.duke.edu/devops/docker-bundle:ruby-2.7 \
		COMMAND
