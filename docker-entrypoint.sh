#!/bin/bash

if [[ -f ./Gemfile.lock ]]; then
    gem install bundler -v "$(tail -1 ./Gemfile.lock | tr -d ' ')"
else
    gem install bundler
fi

bundle "$@"
