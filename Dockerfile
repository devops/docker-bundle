ARG ruby_version

FROM ruby:${ruby_version}

SHELL ["/bin/bash", "-c"]

WORKDIR /usr/src/app

ENV BUNDLE_IGNORE_CONFIG="true" \
	BUNDLE_JOBS="2" \
	BUNDLE_USER_HOME="${GEM_HOME}" \
	HOME="${GEM_HOME}"

RUN chmod -R g=u . $GEM_HOME

VOLUME $GEM_HOME

COPY ./docker-entrypoint.sh /usr/local/sbin/

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]

CMD ["help"]
